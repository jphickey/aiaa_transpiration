import numpy as np
import pdb
import sys
import matplotlib.pyplot as plt
import pickle
myvars=["u","v",'Density','Temperature','Laminar_Viscosity','Eddy_Viscosity']
myFiles=len(myvars)*[None]
xlocations=[0.05, 0.1, 0.15, 0.165,0.185, 0.2,0.205,0.225,0.25,0.275, 0.30, 0.35, 0.4, 0.45]
numberCurves=len(xlocations)

visc=1E-4
if len(sys.argv)>1:
  folder=sys.argv[1]
else:
  folder='./'
output=folder

#PROFILES AT WALL
for ii,vars in enumerate(myvars):
    myFiles[ii]='visitExtract_'+vars+'_profiles.curve'
dataRho  = np.loadtxt(folder+myFiles[2],comments='#',unpack=True)
dataU    = np.loadtxt(folder+myFiles[0],comments='#',unpack=True)
dataV    = np.loadtxt(folder+myFiles[1],comments='#',unpack=True)
dataT    = np.loadtxt(folder+myFiles[3],comments='#',unpack=True)
dataMu   = np.loadtxt(folder+myFiles[4],comments='#',unpack=True)
dataMuT  = np.loadtxt(folder+myFiles[5],comments='#',unpack=True)
nx,nytot = np.shape(dataRho)
ny=nytot/numberCurves

#COMPUTE injection
transRho  = np.loadtxt(folder+'visitExtract_Density_transpiration.curve',comments='#',unpack=True)
transU    = np.loadtxt(folder+'visitExtract_u_transpiration.curve',comments='#',unpack=True)
transV    = np.loadtxt(folder+'visitExtract_v_transpiration.curve',comments='#',unpack=True)
inletRho  = np.loadtxt(folder+'visitExtract_Density_inlet.curve',comments='#',unpack=True)
inletU    = np.loadtxt(folder+'visitExtract_u_inlet.curve',comments='#',unpack=True)


#INITIALIZE
rho  =np.zeros([ny,numberCurves])
u    =np.zeros([ny,numberCurves])
v    =np.zeros([ny,numberCurves])
T    =np.zeros([ny,numberCurves])
mu   =np.zeros([ny,numberCurves])
muT  =np.zeros([ny,numberCurves])

Re_theta =np.zeros(numberCurves)
Re_tau   =np.zeros(numberCurves)
tauW     =np.zeros(numberCurves)
utau     =np.zeros(numberCurves)
delta    =np.zeros(numberCurves)


y=dataRho[0,0:ny]
for ii in range(numberCurves):
    rho[:,ii] =dataRho[1,ii*ny:ii*ny+ny]
    u[:,ii]   =dataU[1,ii*ny:ii*ny+ny]
    v[:,ii]   =dataV[1,ii*ny:ii*ny+ny]
    T[:,ii]   =dataT[1,ii*ny:ii*ny+ny]
    mu[:,ii]  =dataMu[1,ii*ny:ii*ny+ny]
    muT[:,ii] =dataMuT[1,ii*ny:ii*ny+ny]
    tauW[ii]=visc*(u[0,ii]+u[1,ii])/(y[1]+y[0])
    utau[ii]= (tauW[ii]/rho[0,ii])**0.5

Uinfty=np.mean(u[-1,:])
Rhoinfty=np.mean(rho[-1,:])
RhoUinfty=Uinfty*Rhoinfty

for ii in range(numberCurves):
    jj=0
    while jj<ny :
      if u[jj,ii]>0.97*Uinfty:
        delta[ii]=y[jj]
        jj=ny
      jj+=1

print Rhoinfty
print Uinfty
theta=rho*u/RhoUinfty*(1-u/Uinfty)

for ii in range(numberCurves):
    Re_theta[ii]=RhoUinfty * np.trapz(theta[:,ii], y)/visc
    Re_tau[ii]=rho[0,ii] * utau[ii]*delta[ii]/visc
    print Re_theta[ii],Re_tau[ii]

print "Re freestream:", RhoUinfty*delta[3]/visc
print 'lenght=',0.5/delta[3]
print 'distance=',delta[3]

#COMPUTE AVERAGE VELOCITY EXITING Transpiration
Vc=np.trapz(transV[1,:], transV[0,:])/transV[0,-1]
rhoc=np.mean(transRho[1,:])
blowRatio= rhoc*Vc/RhoUinfty
print 'F=',blowRatio


#CREATE PICKLE File
outputPickle=output+"pickled.data"

dict={'y':y,'xlocations':xlocations,"numberCurves":numberCurves,'rho':rho,'u':u,'v':v,'T':T,'F':blowRatio,"Rhoinfty":Rhoinfty,"Uinfty":Uinfty,"utau":utau,"delta":delta,"mu":mu,"muT":muT,"tauW":tauW, "utau":utau,"Re_theta":Re_theta,"Re_tau":Re_tau}

pickle.dump(dict,open( outputPickle, "wb" ) )
quit()
