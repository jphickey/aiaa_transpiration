import numpy as np
import pdb
import sys
sys.path.append("/home/j6hickey/local/visit3_1_0.linux-x86_64/3.1.0/linux-x86_64/lib/site-packages/")
import visit
print 'COMMAND: visit -nowin -cli -s Re.py'
print len(sys.argv)
if len(sys.argv)>1:
  folder=sys.argv[1]
else:
  folder=''
output=folder
myfile=folder+'flow.vtk'

DefineScalarExpression("u", "Momentum[0]/Density")
DefineScalarExpression("v", "Momentum[1]/Density")

xlocations=[0.05, 0.1, 0.15, 0.165,0.185, 0.2,0.205,0.225,0.25,0.275, 0.30, 0.35, 0.4, 0.45]
myvars=["u","v",'Density','Temperature','Laminar_Viscosity','Eddy_Viscosity']
OpenDatabase(myfile, 0)

for vars in myvars:
  AddPlot("Pseudocolor", vars, 1, 1)
  DrawPlots()
  for x in xlocations:
   Query("Lineout", end_point=(x, 0.1, 0),  start_point=(x, 0, 0), use_sampling=0)
  SetActiveWindow(2)
  SaveWindowAtts = SaveWindowAttributes()
  SaveWindowAtts.outputToCurrentDirectory = 1
  SaveWindowAtts.outputDirectory = "."
  SaveWindowAtts.fileName = output+'visitExtract_'+vars+'_profiles'
  SaveWindowAtts.family = 0
  SaveWindowAtts.format = SaveWindowAtts.CURVE
  SetSaveWindowAttributes(SaveWindowAtts)
  SaveWindow()
  DeleteWindow()

  #Transpiration region
  SetActiveWindow(1)
  Query("Lineout", end_point=(0.165, 0.0, 0),  start_point=(0.235, 0, 0), use_sampling=0)
  SetActiveWindow(2)
  SaveWindowAtts = SaveWindowAttributes()
  SaveWindowAtts.outputToCurrentDirectory = 1
  SaveWindowAtts.outputDirectory = "."
  SaveWindowAtts.fileName = output+'visitExtract_'+vars+'_transpiration'
  SaveWindowAtts.family = 0
  SaveWindowAtts.format = SaveWindowAtts.CURVE
  SetSaveWindowAttributes(SaveWindowAtts)
  SaveWindow()
  DeleteWindow()

  #INLET
  SetActiveWindow(1)
  Query("Lineout", end_point=(-0.02, 0.0, 0),  start_point=(-0.02, 0.3, 0), use_sampling=0)
  SetActiveWindow(2)
  SaveWindowAtts = SaveWindowAttributes()
  SaveWindowAtts.outputToCurrentDirectory = 1
  SaveWindowAtts.outputDirectory = "."
  SaveWindowAtts.fileName = output+'visitExtract_'+vars+'_inlet'
  SaveWindowAtts.family = 0
  SaveWindowAtts.format = SaveWindowAtts.CURVE
  SetSaveWindowAttributes(SaveWindowAtts)
  SaveWindow()
  DeleteWindow()
quit()
