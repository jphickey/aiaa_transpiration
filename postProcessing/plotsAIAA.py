
import matplotlib
#matplotlib.use('Agg')
import numpy as np
from myFigs import set_size,set_square,set_stretch,set_almoststretch,set_almoststretch2,set_wider
import numpy as np
import pdb
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle
from scipy.interpolate import interp1d, spline

figType='.pdf'

width = 468
nice_fonts = {
        # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.serif": "FreeSans",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 8,
        "xtick.labelsize": 8,
        "ytick.labelsize": 8,
}

mpl.rcParams.update(nice_fonts)

#DEFINE PATHS
fpath=["../SU2_simulations/testSU2/baselineCase/","../SU2_simulations/testSU2/baselineCase_coarse/","../SU2_simulations/testSU2/baselineCase_fine/","../SU2_simulations/testSU2/injection_p114.5kPa/","../SU2_simulations/testSU2/injection_p114.75kPa/","../SU2_simulations/testSU2/injection_p115kPa/","../SU2_simulations/testSU2/injection_p115kPa_SST/","../SU2_simulations/testSU2/injection_p116kPa/","../SU2_simulations/testSU2/injection_p116kPa_SST/"]
#index=[                   0                                            1                                                      2                                            3                                                4                                                   5                                                   6                                                      7                                               8                   ]
Ltrans=(0.235-0.165)/20.

#LOAD DATA
#dict={'xlocations':xlocations,"numberCurves":numberCurves,'rho':rho,'u':u,'v':v,'T':T,'F':blowRatio,"Rhoinfty":Rhoinfty,"Uinfty":Uinfty,"utau":utau,"delta":delta,"mu":mu,"muT":muT, "tauW":tauW, "utau":utau,"Re_theta":Re_theta,"Re_tau":Re_tau, "deltaT":deltaT}
baseline                =pickle.load( open( fpath[0]+"pickled.data", "rb" ) )
baseline_coarse         =pickle.load( open( fpath[1]+"pickled.data", "rb" ) )
baseline_fine           =pickle.load( open( fpath[2]+"pickled.data", "rb" ) )

F_02_SA                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p114.275kPa/pickled.data", "rb" ) )
F_06_SA                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p114.5kPa/pickled.data", "rb" ) )
#F_10_SA                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p114.75kPa/", "rb" ) )
F_20_SA                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p115.25kPa/pickled.data", "rb" ) )
F_30_SA                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p116kPa/pickled.data", "rb" ) )
#F_06_SST                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p114.5kPa_SST/pickled.data", "rb" ) )
F_10_SST                 =pickle.load( open("../SU2_simulations/testSU2/injection_p114.5kPa_SST/pickled.data","rb"))
F_20_SST                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p115kPa_SST/pickled.data", "rb" ) )
F_30_SST                 =pickle.load( open( "../SU2_simulations/testSU2/injection_p116kPa_SST/pickled.data", "rb" ) )
F_02_DNS                 =pickle.load( open( "./F02_DNS.pickle", "rb" ) )
F_06_DNS                 =pickle.load( open( "./F06_DNS.pickle", "rb" ) )
F_20_DNS                 =pickle.load( open( "./F20_DNS.pickle", "rb" ) )
#SST_115                 =pickle.load( open( fpath[6]+"pickled.data", "rb" ) )

#SA_116                 =pickle.load( open( fpath[7]+"pickled.data", "rb" ) )
#SST_116                =pickle.load( open( fpath[8]+"pickled.data", "rb" ) )

#SA_117                 =pickle.load( open( fpath[9]+"pickled.data", "rb" ) )
#SST_117                =pickle.load( open( fpath[10]+"pickled.data", "rb" ) )


#DEFINE COLOR SCHEME
colors=('k','red','blue','orange')
colors2=('k','blue','blue','blue')
linestyles=('-','--',':')



############################
# TURBULENCE MODELLING
############################
plt.figure(figsize=set_wider(0.85*width))
ax1=plt.subplot(1,2,1)
ax2=plt.subplot(1,2,2)
pos=5 # approx in transpiration

y     =np.asarray(F_20_SST['y'])
U_02_SA      =np.asarray(F_02_SA['u'])
U_06_SA      =np.asarray(F_06_SA['u'])
U_10_SST     =np.asarray(F_10_SST['u'])
U_20_SST     =np.asarray(F_20_SST['u'])
U_30_SST     =np.asarray(F_30_SST['u'])
U_20_SA      =np.asarray(F_20_SA['u'])
U_30_SA      =np.asarray(F_30_SA['u'])
T_02_SA      =np.asarray(F_02_SA['T'])
T_06_SA      =np.asarray(F_06_SA['T'])
T_20_SST     =np.asarray(F_20_SST['T'])
T_20_SA      =np.asarray(F_20_SA['T'])
T_30_SA      =np.asarray(F_30_SA['T'])
U_trans_DNS   =np.asarray(F_20_DNS['uavg'])
ytrans_DNS          =np.asarray(F_20_DNS['y'])
xlocations          = np.asarray(F_06_SA['xlocations'])


pos=5   #baseline for paper
print (xlocations[pos]-0.165)/Ltrans+30

ax1.plot(U_06_SA[:,pos]/max(U_06_SA[:,pos]),y[:]/Ltrans,':k',lw=0.5)
ax1.plot(U_30_SA[:,pos]/max(U_30_SA[:,pos]),y[:]/Ltrans,':k',lw=0.5)
ax1.plot(U_20_SA[:,pos]/max(U_20_SA[:,pos]),y[:]/Ltrans,color=colors[0],label='SA',linestyle=linestyles[0],lw=2.5)
ax1.plot(U_10_SST[:,pos]/max(U_10_SST[:,pos]),y[:]/Ltrans,'-.r',lw=0.5)
ax1.plot(U_30_SST[:,pos]/max(U_30_SST[:,pos]),y[:]/Ltrans,'-.r',lw=0.5)
ax1.plot(U_20_SST[:,pos]/max(U_20_SST[:,pos]),y[:]/Ltrans,color=colors[1],label='SST',linestyle=linestyles[1],lw=2.5)
ax1.scatter(U_trans_DNS[3,::8],ytrans_DNS[::8], s=17, facecolors='none', edgecolors='b',label="DNS")
#DNSpos=3 at mid domain

pos=9
print (xlocations[pos]-0.165)/Ltrans+30
ax2.plot(U_06_SA[:,pos]/max(U_06_SA[:,pos]),y[:]/Ltrans,':k',lw=0.5)
ax2.plot(U_30_SA[:,pos]/max(U_30_SA[:,pos]),y[:]/Ltrans,':k',lw=0.5)
ax2.plot(U_20_SA[:,pos]/max(U_20_SA[:,pos]),y[:]/Ltrans,color=colors[0],label='SA',linestyle=linestyles[0],lw=2.5)
ax2.plot(U_20_SST[:,pos]/max(U_20_SST[:,pos]),y[:]/Ltrans,color=colors[1],label='SST',linestyle=linestyles[1],lw=2.5)
ax2.plot(U_10_SST[:,pos]/max(U_10_SST[:,pos]),y[:]/Ltrans,'-.r',lw=0.5)
ax2.plot(U_30_SST[:,pos]/max(U_30_SST[:,pos]),y[:]/Ltrans,'-.r',lw=0.5)
ax2.scatter(U_trans_DNS[-1,::8],ytrans_DNS[::8], s=17, facecolors='none', edgecolors='b')
ax1.legend()
ax2.set_yticklabels([])
ax1.set_xlabel(r'$u/u_\infty$')
ax1.set_ylabel(r'$y/\delta_i$')
ax2.set_xlabel(r'$u/u_\infty$')
ax1.set_ylim(0,2.5)
ax2.set_ylim(0,2.5)

plt.tight_layout()
plt.savefig("figs/turbulenceCompare"+figType)
plt.show()



############################
# TURBULENCE MODELLING
############################
plt.figure(figsize=set_wider(0.85*width))
ax1=plt.subplot(1,2,1)
ax2=plt.subplot(1,2,2)
pos=5 # approx in transpiration

y     =np.asarray(F_20_SST['y'])
U_02_SA      =np.asarray(F_02_SA['muT'])
U_06_SA      =np.asarray(F_06_SA['muT'])
U_10_SST     =np.asarray(F_10_SST['muT'])
U_20_SST     =np.asarray(F_20_SST['muT'])
U_30_SST     =np.asarray(F_30_SST['muT'])
U_20_SA      =np.asarray(F_20_SA['muT'])
U_30_SA      =np.asarray(F_30_SA['muT'])

xlocations          = np.asarray(F_06_SA['xlocations'])


pos=5  #position for paper
print (xlocations[pos]-0.165)/Ltrans+30
ax1.plot(U_06_SA[:,pos],y[:]/Ltrans,':k',lw=0.5)
ax1.plot(U_30_SA[:,pos],y[:]/Ltrans,':k',lw=0.5)
ax1.plot(U_20_SA[:,pos],y[:]/Ltrans,color=colors[0],label='SA',linestyle=linestyles[0],lw=2.5)
ax1.plot(U_10_SST[:,pos],y[:]/Ltrans,'-.r',lw=0.5)
ax1.plot(U_30_SST[:,pos],y[:]/Ltrans,'-.r',lw=0.5)
ax1.plot(U_20_SST[:,pos],y[:]/Ltrans,color=colors[1],label='SST',linestyle=linestyles[1],lw=2.5)

pos=9
print (xlocations[pos]-0.165)/Ltrans+30
ax2.plot(U_06_SA[:,pos],y[:]/Ltrans,':k',lw=0.5)
ax2.plot(U_30_SA[:,pos],y[:]/Ltrans,':k',lw=0.5)
ax2.plot(U_20_SA[:,pos],y[:]/Ltrans,color=colors[0],label='SA',linestyle=linestyles[0],lw=2.5)
ax2.plot(U_20_SST[:,pos],y[:]/Ltrans,color=colors[1],label='SST',linestyle=linestyles[1],lw=2.5)
ax2.plot(U_10_SST[:,pos],y[:]/Ltrans,'-.r',lw=0.5)
ax2.plot(U_30_SST[:,pos],y[:]/Ltrans,'-.r',lw=0.5)

ax1.legend()
ax2.set_yticklabels([])
ax1.set_xlabel(r'$\mu_t$')
ax1.set_ylabel(r'$y/\delta_i$')
ax2.set_xlabel(r'$\mu_t$')
ax1.set_ylim(0,3)
ax2.set_ylim(0,3)

plt.tight_layout()
plt.savefig("figs/turbulenceCompare_mu"+figType)
plt.show()
quit()

############################
# Comparative Grid convergence
############################
plt.figure(figsize=set_wider(0.75*width))
ax1=plt.subplot(1,2,1)
ax2=plt.subplot(1,2,2)
pos=5 # approx in transpiration

Ubase     =np.asarray(baseline['u'])
Tbase     =np.asarray(baseline['T'])
ybase     =np.asarray(baseline['y'])
Ucoarse   =np.asarray(baseline_coarse['u'])
Tcoarse   =np.asarray(baseline_coarse['T'])
ycoarse   =np.asarray(baseline_coarse['y'])
Ufine     =np.asarray(baseline_fine['u'])
Tfine	  =np.asarray(baseline_fine['T'])
yfine	  =np.asarray(baseline_fine['y'])



ax1.plot(Ucoarse[:,pos]/max(Ucoarse[:,pos]),ycoarse[:],color=colors[1],label='Coarse',linestyle=linestyles[1])
ax1.plot(Ubase[:,pos]/max(Ubase[:,pos]),ybase[:],color=colors[0],label='Baseline',linestyle=linestyles[0])
ax1.plot(Ufine[:,pos]/max(Ufine[:,pos]),yfine[:],color=colors[2],label='Fine',linestyle=linestyles[2])

ax2.plot(Tcoarse[:,pos]/max(Tcoarse[:,pos]),ycoarse[:],color=colors[1],linestyle=linestyles[1])
ax2.plot(Tbase[:,pos]/max(Tbase[:,pos]),ybase[:],color=colors[0],linestyle=linestyles[0])
ax2.plot(Tfine[:,pos]/max(Tfine[:,pos]),yfine[:],color=colors[2],linestyle=linestyles[2])

ax1.legend()
ax2.set_yticklabels([])
ax1.set_xlabel(r'$u/u_\infty$')
ax1.set_ylabel(r'$y$')
ax2.set_xlabel(r'$T/T_\infty$')
ax1.set_ylim(0,0.0045)
ax2.set_ylim(0,0.0045)

plt.tight_layout()
plt.savefig("figs/gridConvergence"+figType)
plt.close()



############################
# Mean evolution
############################
plt.figure(figsize=set_size(width))
ax1=plt.subplot(3,1,1)
ax2=plt.subplot(3,1,2)
ax3=plt.subplot(3,1,3)

U_trans02         = np.asarray(F_02_SA['u'])
U_trans         = np.asarray(F_06_SA['u'])
U_trans20         = np.asarray(F_20_SA['u'])
T_trans         = np.asarray(F_06_SA['T'])
ytrans          = np.asarray(F_06_SA['y'])
U_trans_delta20   = np.asarray(F_20_SA['delta'])
U_trans_delta02   = np.asarray(F_02_SA['delta'])
U_trans_delta   = np.asarray(F_06_SA['delta'])
deltaX          = np.asarray(F_06_SA['xlocations'])


#deltaX = [0.05, 0.1, 0.15, 0.165,0.185, 0.2,0.205,0.225,0.25,0.275, 0.30, 0.35, 0.4, 0.45]
deltaX = (deltaX-0.165)/Ltrans + 30
#index = [  0    1     2      3     4     5    6     7    8     9    10     11   12]
usedPosition = [0,1,2,3,4,6,7,8,9,10,11,12]

for pos in usedPosition:
# approx in transpiration
    multi = 3.0/((U_trans[-1,pos]/max(U_trans[:,pos]))-(U_trans[0,pos]/max(U_trans[:,pos])))
    ax2.plot(((multi)*(U_trans[:,pos]/max(U_trans[:,pos]))+deltaX[pos]-multi),ytrans[:]/Ltrans,color='k')
    multi = 3.0/((U_trans02[-1,pos]/max(U_trans02[:,pos]))-(U_trans02[0,pos]/max(U_trans02[:,pos])))
    ax1.plot(((multi)*(U_trans02[:,pos]/max(U_trans02[:,pos]))+deltaX[pos]-multi),ytrans[:]/Ltrans,color='k')
    ax3.plot(((multi)*(U_trans20[:,pos]/max(U_trans20[:,pos]))+deltaX[pos]-multi),ytrans[:]/Ltrans,color='k')
    #ax4.plot((multi)*(T_trans[:,pos]/max(T_trans[:,pos]))+deltaX[pos]-multi,ytrans[:],color=colors[1])

#create smooth curve for boundary layers

x = deltaX[usedPosition]
y = U_trans_delta[usedPosition]/Ltrans
xnew = np.linspace(10,deltaX[-2], num=1000, endpoint=True)
f3 = interp1d(x,y,kind='cubic')
ax2.plot(xnew,f3(xnew),':k')
ax2.fill_between(xnew,f3(xnew),alpha=0.1)
ax2.plot([30,50],[0,0],'b',lw=3)

print deltaX

x = deltaX[usedPosition]
y = U_trans_delta02[usedPosition]/Ltrans
xnew = np.linspace(10,deltaX[-2], num=1000, endpoint=True)
f3 = interp1d(x,y,kind='cubic')
ax1.plot(xnew,f3(xnew),':k')
ax1.fill_between(xnew,f3(xnew),alpha=0.1)
ax1.plot([30,50],[0,0],'b',lw=3)


y = U_trans_delta20[usedPosition]/Ltrans
xnew = np.linspace(20,deltaX[-2], num=1000, endpoint=True)
f3 = interp1d(x[1:],y[1:],kind='cubic')
ax3.plot(x[1:],y[1:],':k')
ax3.fill_between(xnew,f3(xnew),alpha=0.1)
ax3.plot([30,50],[0,0],'b',lw=3)
ax1.text(21,2. , r'RANS $F=0.2\%$',bbox=dict(facecolor='white', alpha=0.65))
ax2.text(21,2. , r'RANS $F=0.6\%$',bbox=dict(facecolor='white', alpha=0.65))
ax3.text(21,2 , r'RANS $F=2.0\%$',bbox=dict(facecolor='white', alpha=0.65))
ax1.set_ylabel(r'$y/\delta_i$')
ax2.set_ylabel(r'$y/\delta_i$')
ax3.set_ylabel(r'$y/\delta_i$')
ax3.set_xlabel(r'$x/\delta_i$')
ax1.set_xticklabels([])
ax2.set_xticklabels([])
ax1.set_ylim(0,2.6)
ax1.set_xlim(20,65)
ax2.set_ylim(0,2.6)
ax2.set_xlim(20,65)
ax3.set_ylim(0,2.6)
ax3.set_xlim(20,65)
#ax2.set_ylim(0,0.01)
#ax3.set_ylim(0,0.01)
#ax4.set_ylim(0,0.01)
#ax1.set_xlim(0,0.4)
#ax2.set_xlim(0,0.4)
#ax3.set_xlim(0,0.4)
#ax4.set_xlim(0,0.4)


plt.tight_layout()
plt.savefig("figs/meanEvolution"+figType)
plt.close()



############################
# Eddy Viscosity RANS comparison
############################

plt.figure(figsize=set_size(width))

eddy_viscosity_115_SA    = np.asarray(F_20_SA['muT'])
#eddy_viscosity_115_SST   = np.asarray(SST_115['muT'])
y115_SA                  = np.asarray(F_20_SA['y'])
#y115_SST                 = np.asarray(SST_115['y'])

#eddy_viscosity_116_SA    = np.asarray(SA_116['muT'])
#eddy_viscosity_116_SST   = np.asarray(SST_116['muT'])
#y116_SA                  = np.asarray(SA_116['y'])
#y116_SST                 = np.asarray(SST_116['y'])

#eddy_viscosity_117_SA    = np.asarray(SA_117['muT'])
#eddy_viscosity_117_SST   = np.asarray(SST_117['muT'])
#y117_SA                  = np.asarray(SA_117['y'])
#y117_SST                 = np.asarray(SST_117['y'])

pos = 11
#midpoint of the flow ===> 0.275m

ax1=plt.subplot(1,1,1)

#ax1.plot(eddy_viscosity_115_SA[:,pos], y115_SA[:], color=colors[0],label='SA, F=0.06',linestyle=linestyles[0])
#ax1.plot(eddy_viscosity_115_SST[:,pos], y115_SST[:], color=colors[1],label='SST, F=0.06',linestyle=linestyles[1])
#ax1.plot(eddy_viscosity_116_SA[:,pos], y116_SA[:], color=colors[0],label='SA, F=0.12',linestyle=linestyles[0])
#ax1.plot(eddy_viscosity_116_SST[:,pos], y116_SST[:], color=colors[2],label='SST, F=0.12',linestyle=linestyles[2])

ax1.legend()
ax1.set_ylabel(r'$y$')
ax1.set_xlabel(r'x')
ax1.set_ylim(0,0.0075)
plt.tight_layout()
plt.savefig("figs/eddyViscosity"+figType)
plt.close()


############################
# Turbulent Viscosity RANS vs. DNS
############################
plt.figure(figsize=set_size(width))

muT_RANS         = np.asarray(F_06_SA['muT'])
yRANS            = np.asarray(F_06_SA['y'])

contours = 10
levels =  np.zeros(contours)
space = 0.006/contours
for level in range(contours):
    levels[level]=space*level+space

levels = levels.round(decimals=5)

cntr1 = ax1.contour(deltaX, yRANS, muT_RANS, levels)

h1,l1 = cntr1.legend_elements("\mu_{eddy}")

ax1.legend(h1,l1,loc=5,bbox_to_anchor=(1.2,0.5))

ax1.set_title('RANS Eddy Viscosity')
ax2.set_title('DNS Eddy Viscosity')
ax2.set_yticklabels([])
ax1.set_ylabel(r'$y$')
ax2.set_ylabel(r'$y$')
ax2.set_xlabel(r'x')
ax1.set_ylim(0,0.0075)
ax2.set_ylim(0,0.0075)

plt.tight_layout()
plt.savefig("figs/turbulentViscosity"+figType)
plt.close()


############################
# Transpiration midpoint velocity
############################
plt.figure(figsize=set_wider(0.87*width))
ax1=plt.subplot(1,3,1)
ax2=plt.subplot(1,3,2)
ax3=plt.subplot(1,3,3)

pos_rans=[3,6,9]   #30, 41.428571,61.428
#[ -2.85714286  11.42857143  25.71428571  30.          35.71428571  40.          41.42857143  47.14285714  54.28571429  61.42857143  68.57142857  82.85714286  97.14285714 111.42857143]
U_trans         = np.asarray(F_20_SA['u'])
ytrans          = np.asarray(F_20_SA['y'])

pos_dns=[1,3,6]
#DNS  mydict={'y':y,'xlocations':xpos,'uavg':uavg[dns_xpos,:],'vavg':vavg[dns_xpos,:]}
#DNS [ 25.71428571   ,  30,      35.71428571   ,       41.42857143,  47.14285714,  54.28571429 , 59.42857143,    ])
U_trans_DNS         =np.asarray(F_20_DNS['uavg'])
ytrans_DNS          =np.asarray(F_20_DNS['y'])

ax1.text(0.1,1.35 , r'$x=30$',bbox=dict(facecolor='white', alpha=0.65))
ax2.text(0.1,2. , r'$x=41$',bbox=dict(facecolor='white', alpha=0.65))
ax3.text(0.1,2. , r'$x=59$',bbox=dict(facecolor='white', alpha=0.65))
ax1.plot(U_trans[:,pos_rans[0]]/max(U_trans[:,pos_rans[0]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax1.plot(U_trans_DNS[pos_dns[0],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])
ax2.plot(U_trans[:,pos_rans[1]]/max(U_trans[:,pos_rans[1]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax2.plot(U_trans_DNS[pos_dns[1],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])

ax3.plot(U_trans[:,pos_rans[2]]/max(U_trans[:,pos_rans[2]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax3.plot(U_trans_DNS[pos_dns[2],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])
ax1.set_ylim(0,2.5)
ax2.set_ylim(0,2.5)
ax3.set_ylim(0,2.5)
ax2.set_yticklabels([])
ax3.set_yticklabels([])
ax1.legend()
ax1.set_xlabel(r'$u/u_\infty$')
ax2.set_xlabel(r'$u/u_\infty$')
ax3.set_xlabel(r'$u/u_\infty$')
ax1.set_ylabel(r'$y$')
plt.tight_layout()
plt.savefig("figs/transpirationVelocity"+figType)



############################
# Transpiration midpoint velocity at F=0.2
############################
plt.figure(figsize=set_wider(0.87*width))
ax1=plt.subplot(1,3,1)
ax2=plt.subplot(1,3,2)
ax3=plt.subplot(1,3,3)

pos_rans=[3,6,9]   #30, 41.428571,61.428
#[ -2.85714286  11.42857143  25.71428571  30.          35.71428571  40.          41.42857143  47.14285714  54.28571429  61.42857143  68.57142857  82.85714286  97.14285714 111.42857143]
U_trans         = np.asarray(F_02_SA['u'])
ytrans          = np.asarray(F_02_SA['y'])

pos_dns=[1,3,6]
#DNS  mydict={'y':y,'xlocations':xpos,'uavg':uavg[dns_xpos,:],'vavg':vavg[dns_xpos,:]}
#DNS [ 25.71428571   ,  30,      35.71428571   ,       41.42857143,  47.14285714,  54.28571429 , 59.42857143,    ])
U_trans_DNS         =np.asarray(F_02_DNS['uavg'])
ytrans_DNS          =np.asarray(F_02_DNS['y'])

ax1.text(0.1,1.35 , r'$x=30$',bbox=dict(facecolor='white', alpha=0.65))
ax2.text(0.1,2. , r'$x=41$',bbox=dict(facecolor='white', alpha=0.65))
ax3.text(0.1,2. , r'$x=59$',bbox=dict(facecolor='white', alpha=0.65))
ax1.plot(U_trans[:,pos_rans[0]]/max(U_trans[:,pos_rans[0]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax1.plot(U_trans_DNS[pos_dns[0],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])
ax2.plot(U_trans[:,pos_rans[1]]/max(U_trans[:,pos_rans[1]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax2.plot(U_trans_DNS[pos_dns[1],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])

ax3.plot(U_trans[:,pos_rans[2]]/max(U_trans[:,pos_rans[2]]),ytrans[:]/Ltrans,color=colors[0],label='RANS',linestyle=linestyles[0])
ax3.plot(U_trans_DNS[pos_dns[2],:],ytrans_DNS[:],color=colors[1],label='DNS',linestyle=linestyles[0])
ax1.set_ylim(0,2.5)
ax2.set_ylim(0,2.5)
ax3.set_ylim(0,2.5)
ax2.set_yticklabels([])
ax3.set_yticklabels([])
ax1.legend()
ax1.set_xlabel(r'$u/u_\infty$')
ax2.set_xlabel(r'$u/u_\infty$')
ax3.set_xlabel(r'$u/u_\infty$')
ax1.set_ylabel(r'$y$')
plt.tight_layout()
plt.savefig("figs/transpirationVelocity_F02"+figType)


############################
# Transpiration midpoint velocity
############################
plt.figure(figsize=set_size(width))
pos=5 # approx in transpiration

T_trans         = np.asarray(F_06_SA['T'])
ytrans          = np.asarray(F_06_SA['y'])

plt.plot(T_trans[:,pos]/max(T_trans[:,pos]),ytrans[:],color=colors[0],label='RANS',linestyle=linestyles[0])

plt.legend()
plt.xlabel(r'$T/T_\infty$')
plt.ylabel(r'$y$')
plt.ylim(0,0.0075)


plt.tight_layout()
plt.savefig("figs/transpirationTemperature"+figType)
plt.show()
