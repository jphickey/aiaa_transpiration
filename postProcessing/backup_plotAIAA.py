import numpy as np
from myFigs import set_size,set_square,set_stretch,set_almoststretch,set_almoststretch2
import numpy as np
import pdb
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import pickle

figType='.pdf'

width = 468
nice_fonts = {
        # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 8,
        "xtick.labelsize": 8,
        "ytick.labelsize": 8,
}

mpl.rcParams.update(nice_fonts)

#DEFINE PATHS
fpath=["../SU2_simulations/testSU2/baselineCase/","../SU2_simulations/testSU2/baselineCase_coarse/","../SU2_simulations/testSU2/baselineCase_fine/","../SU2_simulations/testSU2/injection_p114.5kPa/","../SU2_simulations/testSU2/injection_p114.75kPa/"]


#LOAD DATA
#dict={'xlocations':xlocations,"numberCurves":numberCurves,'rho':rho,'u':u,'v':v,'T':T,'F':blowRatio,"Rhoinfty":Rhoinfty,"Uinfty":Uinfty,"utau":utau,"delta":delta,"mu":mu,"muT":muT, "tauW":tauW, "utau":utau,"Re_theta":Re_theta,"Re_tau":Re_tau}
baseline         =pickle.load( open( fpath[0]+"pickled.data", "rb" ) )
baseline_coarse  =pickle.load( open( fpath[1]+"pickled.data", "rb" ) )


#DEFINE COLOR SCHEME
colors=('k','red','red','red')
colors2=('k','blue','blue','blue')
linestyles=('-',':','--','-.')

############################
# Comparative Grid convergence
############################
plt.figure(figsize=set_size(width))
ax1=plt.subplot(1,2,1)
ax2=plt.subplot(1,2,2)
pos=5 # approx in transpiration

Ubase=np.asarray(baseline['u'])
Tbase=np.asarray(baseline['T'])
ybase=np.asarray(baseline['y'])
Ucoarse=np.asarray(baseline_coarse['u'])
Tcoarse=np.asarray(baseline_coarse['T'])
ycoarse=np.asarray(baseline_coarse['y'])

ax1.plot(Ubase[:,pos]/max(Ubase[:,pos]),ybase[:],color=colors[0],label='baseline')
ax1.plot(Ucoarse[:,pos]/max(Ucoarse[:,pos]),ycoarse[:],color=colors[1],label='coarse')
ax2.plot(Tbase[:,pos]/max(Tbase[:,pos]),ybase[:],color=colors[0])
ax2.plot(Tcoarse[:,pos]/max(Tcoarse[:,pos]),ycoarse[:],color=colors[1])
ax1.legend()
ax2.set_yticklabels([])
ax1.set_xlabel(r'$u/u_\infty$')
ax1.set_ylabel(r'$y$')
ax2.set_xlabel(r'$T/T_\infty$')
ax1.set_ylim(0,0.0075)
ax2.set_ylim(0,0.0075)
plt.tight_layout()
plt.savefig("figs/gridConvergence"+figType)
plt.show()
