import numpy as np
import matplotlib.pyplot as plt

#from cluster import cluster
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.image import NonUniformImage
import matplotlib
import array
import pdb
import os.path
import scipy.io
import pickle
import gc
from scipy import signal
from myFigs import set_size,set_square,set_stretch,set_almostsquare,set_rect
Version="0.1"



width = 468


nice_fonts = {
# Use LaTex to write all text
"text.usetex": True,
"font.family": "sans-serif",
"font.serif": "FreeSans",
# Use 10pt font in plots, to match 10pt font in document
"axes.labelsize": 10.5,
"font.size": 10.5,
# Make the legend/label fonts a little smaller
"legend.fontsize": 8,
"xtick.labelsize": 10.5,
"ytick.labelsize": 10.5,
}

matplotlib.rcParams.update(nice_fonts)

infile = open('pickledData.p','rb')
data = pickle.load(infile)
nslices = data['nslices']
nfiles = data['nfiles']
x = data['x']
avgU = data['avgU']

avgT = data['avgT']

avgH_1 = data['avgH_1']
avgH_2 = data['avgH_2']
################

data2=pickle.load( open( 'xyz.p', "rb" ) )
#data={'nbslices':len(sliceArray),'x':x,'y':y,'u':u[:,:,sliceArray],'T':T[:,:,sliceArray]}


y=data2['y']
X, Y = np.meshgrid(x,y[0:150])


#DEFINE PATHS
fpath=["../SU2_simulations/testSU2/injection_p114.75kPa/","../SU2_simulations/testSU2/injection_p115.25kPa/"]


#LOAD DATA
#dict={'xlocations':xlocations,"numberCurves":numberCurves,'rho':rho,'u':u,'v':v,'T':T,'F':blowRatio,"Rhoinfty":Rhoinfty,"Uinfty":Uinfty,"utau":utau,"delta":delta,"mu":mu,"muT":muT, "tauW":tauW, "utau":utau,"Re_theta":Re_theta,"Re_tau":Re_tau}

Ltrans=(0.235-0.165)/20.
delta=Ltrans
x, y, z,T = np.loadtxt('./Temperature_F006_xyz.xyz', unpack=True)
sort_index = np.argsort(y)
ynew=y[sort_index]/delta
Tnew=T[sort_index]
xnew=(x[sort_index]-0.165)/delta + 30

xtemp=x[sort_index]
#Count the number of zeros (HARD coded)
nx=204
ny=97
T_F006=np.zeros([nx,ny])
yrans=np.zeros(ny)
Tmax=300
Tmin=155
xtemp=xnew[:nx]
sort_index = np.argsort(xtemp)
xrans=xtemp[sort_index ]

for ii in range(ny):
    yrans[ii]=ynew[ii*nx]
    Tsec=Tnew[ii*nx:ii*nx+nx]
    sort_index = np.argsort(xnew[ii*nx:ii*nx+nx])
    T_F006[:,ii]=Tsec[sort_index]


x, y, z,T = np.loadtxt('./Temperature_F02_xyz.xyz', unpack=True)
sort_index = np.argsort(y)
ynew=y[sort_index]/delta
Tnew=T[sort_index]
xnew=x[sort_index]/delta

xtemp=x[sort_index]
#Count the number of zeros (HARD coded)
nx=204
ny=97
T_F02=np.zeros([nx,ny])
yrans=np.zeros(ny)
Tmax=300
Tmin=155
xtemp=xnew[:nx]


for ii in range(ny):
    yrans[ii]=ynew[ii*nx]
    Tsec=Tnew[ii*nx:ii*nx+nx]
    sort_index = np.argsort(xnew[ii*nx:ii*nx+nx])
    T_F02[:,ii]=Tsec[sort_index]


x, y, z,T = np.loadtxt('./Temperature_F002_xyz.xyz', unpack=True)
sort_index = np.argsort(y)
ynew=y[sort_index]/delta
Tnew=T[sort_index]
xnew=x[sort_index]/delta

xtemp=x[sort_index]
#Count the number of zeros (HARD coded)
nx=204
ny=97
T_F002=np.zeros([nx,ny])

for ii in range(ny):
    Tsec=Tnew[ii*nx:ii*nx+nx]
    sort_index = np.argsort(xnew[ii*nx:ii*nx+nx])
    T_F002[:,ii]=Tsec[sort_index]




#RANS SIMULATION
fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True,figsize=set_size(width))
norm2 = matplotlib.colors.Normalize(vmin=0.55, vmax=1., clip=False)
levels = np.linspace(0.55, 1.0, 11)

XRANS,YRANS=np.meshgrid(xrans,yrans)
col = ax1.contourf(XRANS,YRANS,np.transpose(T_F002/Tmax),levels=levels,norm=norm2,cmap='RdBu_r')
col = ax2.contourf(XRANS,YRANS,np.transpose(T_F006/Tmax),levels=levels,norm=norm2,cmap='RdBu_r')
col = ax3.contourf(XRANS,YRANS,np.transpose(T_F02/Tmax),levels=levels,norm=norm2,cmap='RdBu_r')
ax1.plot([30,50],[0,0],'b',lw=4)
ax2.plot([30,50],[0,0],'b',lw=4)
ax3.plot([30,50],[0,0],'b',lw=4)
ax1.set_xlim(25,55)
ax1.set_ylim(0,2.2)
ax2.set_xlim(25,55)
ax2.set_ylim(0,2.2)
ax3.set_xlim(25,55)
ax3.set_ylim(0,2.2)
ax3.text(.01,.8 , r'RANS $F=2\%$', color='white',transform=ax3.transAxes, size=12, weight='bold')
ax2.text(.01,.8 , r'RANS $F=0.6\%$',color='white', transform=ax2.transAxes, size=12, weight='bold')
ax1.text(.01,.8 , r'RANS $F=0.2\%$',color='white', transform=ax1.transAxes, size=12, weight='bold')
ax1.set_yticks(np.arange(3))
#ax1.yticks(np.arange(0, 3, 3))

ax1.set_ylabel(r'y/$\delta_i$', rotation=0)
ax2.set_ylabel(r'y/$\delta_i$', rotation=0)
ax3.set_ylabel(r'y/$\delta_i$', rotation=0)
ax3.set_xlabel(r'x/$\delta_i$')
plt.tight_layout()

v = np.linspace(0.5, 1.0, 6, endpoint=True)
cb = fig.colorbar(col, ax=(ax2, ax3, ax1),ticks=[0.5, 0.6, 0.7,0.8,0.9,1])
cb.set_label(r'$T$', rotation=0)

plt.savefig('RANSuniformT.pdf')

plt.show()





#DNS SIMULATION
fig, (ax2, ax3, ax1) = plt.subplots(3, sharex=True,figsize=set_size(width))
#plt.figure(figsize=set_size(width))
#ax1=plt.subplot(3,1,1)
#ax2=plt.subplot(3,1,2)
#ax3=plt.subplot(3,1,3)

norm2 = matplotlib.colors.Normalize(vmin=0.55, vmax=1., clip=False)
levels = np.linspace(0.55, 1.0, 11)
# UNIFORM 1
ii=0
norm=nslices[ii]*nfiles[ii]

avgT[:,:,ii] = np.clip(avgT[:,:,ii], 0, norm)

col = ax1.contourf(X,Y,np.transpose(avgT[:,:,ii]/norm),levels=levels,norm=norm2,cmap='RdBu_r')
#ax1.plot(x,avgH_2[:,ii]/norm,'k')

# UNIFORM 3
ii=2
norm=nslices[ii]*nfiles[ii]
avgT[:,:,ii] = np.clip(avgT[:,:,ii], 0, norm)
col = ax3.contourf(X,Y,np.transpose(avgT[:,:,ii]/norm),levels=levels,norm=norm2,cmap='RdBu_r')
#ax3.plot(x,avgH_2[:,ii]/norm,'k')

# UNIFORM 2
ii=1
norm=nslices[ii]*nfiles[ii]
avgT[:,:,ii] = np.clip(avgT[:,:,ii], 0, norm)
col = ax2.contourf(X,Y,np.transpose(avgT[:,:,ii]/norm),levels=levels,norm=norm2,cmap='RdBu_r')
#ax2.plot(x,avgH_2[:,ii]/norm,'k')

ax1.plot([30,50],[0,0],'b',lw=4)
ax2.plot([30,50],[0,0],'b',lw=4)
ax3.plot([30,50],[0,0],'b',lw=4)
print np.max(np.transpose(avgT[:,:,:]/norm))
plt.xlabel(r'x/$\delta_i$')

#ax1.set_xlabel('x')
#ax1.set_ylabel('y')

ax1.set_ylabel(r'y/$\delta_i$', rotation=0)
ax2.set_ylabel(r'y/$\delta_i$', rotation=0)
ax3.set_ylabel(r'y/$\delta_i$', rotation=0)

ax1.set_xlim(25,55)
ax1.set_ylim(0,2.2)

ax3.set_xlim(25,55)
ax3.set_ylim(0,2.2)

ax2.set_xlim(25,55)
ax2.set_ylim(0,2.2)

ax1.text(.01,.8 , r'DNS $F=2\%$', color='white',transform=ax1.transAxes, size=12, weight='bold')
ax3.text(.01,.8 , r'DNS $F=0.6\%$',color='white', transform=ax3.transAxes, size=12, weight='bold')
ax2.text(.01,.8 , r'DNS $F=0.2\%$',color='white', transform=ax2.transAxes, size=12, weight='bold')
print np.arange(3)

ax1.set_yticks(np.arange(3))
#ax1.yticks(np.arange(0, 3, 3))
plt.tight_layout()

v = np.linspace(0.5, 1.0, 6, endpoint=True)
cb = fig.colorbar(col, ax=(ax2, ax3, ax1),ticks=[0.5, 0.6, 0.7,0.8,0.9,1])
cb.set_label(r'$T$', rotation=0)

plt.savefig('DNSuniformT.pdf')
plt.show()
