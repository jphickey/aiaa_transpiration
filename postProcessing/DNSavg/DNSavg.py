#!/usr/bin/env python
# This file reads the binary restart file
import matplotlib
import sys
import numpy as np

import matplotlib.pyplot as plt


import array
import pdb
import os.path
import scipy.io
import pickle
from scipy import signal
from myFigs import set_size,set_square,set_stretch,set_almostsquare,set_rect


Version="0.1"


width = 468


nice_fonts = {
        # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.serif": "FreeSans",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 8,
        "xtick.labelsize": 8,
        "ytick.labelsize": 8,
}

matplotlib.rcParams.update(nice_fonts)


files=['uniform2.avg','uniform3.avg','uniform1.avg']
#       f02=     f06=     f20
plt.figure(figsize=set_size(width))
ax1=plt.subplot(3,1,1)
ax2=plt.subplot(3,1,2)
ax3=plt.subplot(3,1,3)

R =7.9365
gamma = 1.4
cv = R/(gamma-1.0)

data2=pickle.load( open( 'xyz.p', "rb" ) )

x=data2['x']
y=data2['y']

Ni=len(x)
Nj=len(y)

X, Y = np.meshgrid(x, y)
########################## averaged data ################################

for myii,f in enumerate(files):
  f2 = open(f, "rb")

  # Header with integer type
  data_type_string = "(4)i4, "
  file_header_int = np.fromfile(f2, dtype = np.dtype(data_type_string),count=1)[0]

  precision_bytes, version, nvars, Ni = file_header_int[:4]
  print precision_bytes

  Ntot = Ni * Nj

  data_type_string = "(1)f"+repr(precision_bytes)+", "

  print "Reading avg file...\n"
  prop = np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot).reshape(Ni, Nj)
  rhoavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  rho_uavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  rho_vavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  rho_wavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  rho_Eavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  uavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  vavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  wavg = np.copy(prop[:Ni,:Nj])
  del prop
  prop = np.reshape(np.fromfile(f2, dtype = np.dtype(data_type_string),count=Ntot), [Ni, Nj])
  Pavg = np.copy(prop[:Ni,:Nj])
  del prop


  xpos=np.asarray([ 25.71428571   ,  30,      35.71428571   ,       41.42857143,  47.14285714,  54.28571429 , 59.42857143,    ])
  dns_xpos=np.zeros(len(xpos))
  for ii,value in enumerate(xpos):
    dns_xpos[ii] = int((np.abs(x - value)).argmin())

  if myii==0:
    delta_x=np.zeros((2550-200)/50)
    delta_y=np.zeros((2550-200)/50)
  counter=0
  for ii in range(200,2550,50):
      delta_x[counter]=x[ii]
      jj=0
      while jj<165:
          if uavg[ii,jj]>0.975 :
              delta_y[counter]=y[jj]
              jj=200
              #if myii==1: pdb.set_trace()
          else:
              jj+=1
      counter+=1
  delta_x[-1]=60
  for mypos in dns_xpos:
    if myii==0:
      ax1.plot(3*uavg[int(mypos),:]+x[int(mypos)]-3,y,color='k')
      ax1.plot([30,50],[0,0],'b',lw=3)
      ax1.plot(delta_x,delta_y,':k')
      ax1.fill_between(delta_x,delta_y,alpha=0.01,color='blue')
      filename='F02_DNS.pickle'
    if myii==1:
      ax2.plot(3*uavg[int(mypos),:]+x[int(mypos)]-3,y,color='k')
      ax2.plot([30,50],[0,0],'b',lw=3)
      ax2.plot(delta_x,delta_y,':k')
      ax2.fill_between(delta_x,delta_y,alpha=0.01,color='blue')
      filename='F06_DNS.pickle'
    if myii==2:
      ax3.plot(3*uavg[int(mypos),:]+x[int(mypos)]-3,y,color='k')
      ax3.plot([30,50],[0,0],'b',lw=3)
      ax3.plot(delta_x,delta_y,':k')
      ax3.fill_between(delta_x,delta_y,alpha=0.01,color='blue')
      filename='F20_DNS.pickle'

  dns_xpos=dns_xpos.astype(int)
  mydict={'y':y,'xlocations':xpos,'uavg':uavg[dns_xpos,:],'vavg':vavg[dns_xpos,:]}
  pickle.dump(mydict,open( filename, "wb" ) )

ax1.text(21,2. , r'DNS $F=0.2\%$',bbox=dict(facecolor='white', alpha=0.65))
ax2.text(21,2. , r'DNS $F=0.6\%$',bbox=dict(facecolor='white', alpha=0.65))
ax3.text(21,2 , r'DNS $F=2.0\%$',bbox=dict(facecolor='white', alpha=0.65))
ax1.set_ylabel(r'$y/\delta_i$')
ax2.set_ylabel(r'$y/\delta_i$')
ax3.set_ylabel(r'$y/\delta_i$')
ax3.set_xlabel(r'$x/\delta_i$')
ax1.set_xticklabels([])
ax2.set_xticklabels([])
ax1.set_ylim(0,2.6)
ax1.set_xlim(20,60)
ax2.set_ylim(0,2.6)
ax2.set_xlim(20,60)
ax3.set_ylim(0,2.6)
ax3.set_xlim(20,60)
plt.tight_layout()
plt.savefig("../figs/meanEvolutionDNS.pdf")
plt.show()
############
##  PLOTS ##
############

print "Plotting..."
