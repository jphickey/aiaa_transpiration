import pyvista as vtki
import numpy as np
import matplotlib.pyplot as plt

grid = vtki.UnstructuredGrid('./VTK/current_1100/internal.vtu')

#print(grid.points)

#print(grid.cell_arrays)
#print(grid.point_arrays)

t_cell = grid.point_arrays['T']
u_cell = grid.point_arrays['U']
p_cell = grid.point_arrays['p']
position =grid.points

domain1 = np.hstack((position,u_cell))
domain2 = np.column_stack((p_cell, t_cell))
domain = np.hstack((domain1,domain2))
#print(domain1)
#print(domain2)
#print(domain)

#domain index
#[x,y,z,u,v,w,p,t]
#[0 1 2 3 4 5 6 7]




#domain at x=0.165m
domain_int = domain[domain[:,0]==0.165]
domain165 = domain_int[domain_int[:,2]==0.005]
#print(domain165)
#print(len(domain165))



#rho_cell= p_cell/(287.05*t_cell)
rho_domain165 = domain165[:,6]/(287.05*domain165[:,7])
#vis_cell = 3.624e-5*(t_cell/800)**0.7
vis_domain165 = 3.624e-5*(domain165[:,7]/800)**0.7


#numerical intergration of momentum thickness using trapizoid rule

mom_thick = 0
pointer = 0

while pointer < (len(domain165)-1):
	f1 = ((rho_domain165[pointer]*domain165[pointer,3])/(rho_domain165[-1]*domain165[-1,3])) \
	*(1-(domain165[pointer,3]/domain165[-1,3]))
	f2 = ((rho_domain165[pointer+1]*domain165[pointer+1,3])/(rho_domain165[-1]*domain165[-1,3])) \
	*(1-(domain165[pointer+1,3]/domain165[-1,3]))
	dely = domain165[pointer+1,1]-domain165[pointer,1]
	mom_thick = mom_thick + (((f1 +f2)/2)*dely)
	#print(mom_thick)
	pointer += 1



Re_momentum = rho_domain165[-1]*domain165[-1,3]*mom_thick/vis_domain165[-1]

print(Re_momentum)

#grid._add_point_array(Re_x, 'Re_x')

#grid.save('./VTK/current_1100/internal.vtu')



#plt.plot(Re_x[:,1],position[:,1])
#plt.show()

#print(position[:,1])
#print(u_cell[:,1])
#print(Re_x[])

#print(len(position))
#print(len(u_cell))
#print(len(t_cell))
#print(Re)