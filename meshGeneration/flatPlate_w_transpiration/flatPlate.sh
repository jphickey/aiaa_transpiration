#!/bin/bash
#SBATCH --time=23:59:00
#SBATCH --account=def-jphickey
#SBATCH --mem-per-cpu=125G

module purge
module load openfoam/v1906

blockMesh
rhoSimpleFoam
foamToVTK
