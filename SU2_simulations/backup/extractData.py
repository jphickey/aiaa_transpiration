import vtk
import pdb
Filename = 'flow.vtk'
reader = vtk.vtkUnstructuredGridReader()
reader.SetFileName(Filename)
reader.ReadAllScalarsOn()
reader.ReadAllVectorsOn()
reader.Update()
usg = reader.GetOutput()
pdb.set_trace()
vec1 = usg.GetPointData().GetVector('vector1')
