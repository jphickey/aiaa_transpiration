# Visit 3.0.0b log file
ScriptVersion = "3.0.0b"
if ScriptVersion != Version():
    print "This script is for VisIt %s. It may not work with version %s" % (ScriptVersion, Version())
ShowAllWindows()
HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0209269, 1.57277, -0.722906, 0.875401)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.188265, 1.74011, -0.890728, 1.04322)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.390744, 1.94259, -1.09379, 1.24629)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.635744, 2.18759, -1.3395, 1.492)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.932193, 2.48404, -1.63681, 1.7893)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.2909, 2.84274, -1.99655, 2.14904)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.932193, 2.48404, -1.63681, 1.7893)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.635744, 2.18759, -1.3395, 1.492)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.390744, 1.94259, -1.09379, 1.24629)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.188265, 1.74011, -0.890728, 1.04322)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0209269, 1.57277, -0.722906, 0.875401)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.117369, 1.43447, -0.58421, 0.736705)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.231663, 1.32018, -0.469585, 0.62208)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.445729, 0.642787, -0.504574, 0.58709)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0692444, 1.01927, -0.504574, 0.58709)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.183539, 1.13357, -0.619199, 0.701715)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.321835, 1.27186, -0.757895, 0.840411)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.489173, 1.4392, -0.925717, 1.00823)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.691652, 1.64168, -1.12878, 1.2113)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.936651, 1.88668, -1.37449, 1.45701)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.217887, 2.60544, -1.34908, 1.48242)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.217887, 2.60544, -1.34908, 1.48242)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.027113, 2.36044, -1.10337, 1.23671)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.229592, 2.15796, -0.900306, 1.03364)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.39693, 1.99063, -0.732484, 0.865822)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.229592, 2.15796, -0.900306, 1.03364)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.027113, 2.36044, -1.10337, 1.23671)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.217887, 2.60544, -1.34908, 1.48242)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.514336, 2.90189, -1.64639, 1.77972)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.12489, 2.29134, -1.4663, 1.95982)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.828437, 1.99489, -1.16899, 1.66251)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.583438, 1.74989, -0.923281, 1.4168)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.380959, 1.54741, -0.720216, 1.21373)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.21362, 1.38008, -0.552394, 1.04591)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.21362, 1.38008, -0.552394, 1.04591)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.380959, 1.54741, -0.720216, 1.21373)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.583438, 1.74989, -0.923281, 1.4168)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.828437, 1.99489, -1.16899, 1.66251)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.828437, 1.99489, -1.16899, 1.66251)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.12489, 2.29134, -1.4663, 1.95982)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.48359, 2.65005, -1.82604, 2.31956)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.91762, 3.08408, -2.26133, 2.75484)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.4428, 3.60926, -2.78802, 3.28154)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.91762, 3.08408, -2.26133, 2.75484)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.48359, 2.65005, -1.82604, 2.31956)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.12489, 2.29134, -1.4663, 1.95982)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.828437, 1.99489, -1.16899, 1.66251)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.583438, 1.74989, -0.923281, 1.4168)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.380959, 1.54741, -0.720216, 1.21373)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.380959, 1.54741, -0.720216, 1.21373)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.380959, 1.54741, -0.720216, 1.21373)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

DrawPlots()
HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.646257, 1.28211, -0.864022, 1.06993)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.848736, 1.48459, -1.06709, 1.27299)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.09374, 1.72959, -1.3128, 1.5187)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.39019, 2.02604, -1.6101, 1.81601)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.74889, 2.38475, -1.96984, 2.17575)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.18292, 2.81878, -2.40513, 2.61104)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.74889, 2.38475, -1.96984, 2.17575)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.39019, 2.02604, -1.6101, 1.81601)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.09374, 1.72959, -1.3128, 1.5187)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.848736, 1.48459, -1.06709, 1.27299)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.646257, 1.28211, -0.864022, 1.06993)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.507409, 1.42096, -0.561533, 1.37242)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.130537, 1.79784, -0.57145, 1.3625)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.876843, 1.05153, -0.566492, 1.36746)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.07932, 1.25401, -0.769556, 1.57052)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01526, 1.81623)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.31257, 2.11354)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.97948, 2.15416, -1.67231, 2.47328)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.31257, 2.11354)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01526, 1.81623)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.07932, 1.25401, -0.769556, 1.57052)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.876843, 1.05153, -0.566492, 1.36746)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.709505, 0.884191, -0.398669, 1.19964)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.571209, 0.745895, -0.259973, 1.06094)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.456915, 0.6316, -0.145349, 0.946316)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.362457, 0.537142, -0.0506174, 0.851585)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.284393, 0.459078, 0.0276729, 0.773295)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.362457, 0.537142, -0.0506174, 0.851585)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.456915, 0.6316, -0.145349, 0.946316)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.571209, 0.745895, -0.259973, 1.06094)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.709505, 0.884191, -0.398669, 1.19964)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.876843, 1.05153, -0.566492, 1.36746)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.07932, 1.25401, -0.769556, 1.57052)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01526, 1.81623)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk")
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01889, 1.8126)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01889, 1.8126)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01889, 1.8126)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.3162, 2.10991)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.3162, 2.10991)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.3162, 2.10991)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.97948, 2.15416, -1.67594, 2.46965)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.41351, 2.58819, -2.11123, 2.90494)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.93869, 3.11337, -2.63793, 3.43164)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.41351, 2.58819, -2.11123, 2.90494)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.97948, 2.15416, -1.67594, 2.46965)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.62077, 1.79546, -1.3162, 2.10991)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01889, 1.8126)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.07932, 1.25401, -0.773187, 1.56689)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.876843, 1.05153, -0.570122, 1.36383)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.709505, 0.884191, -0.4023, 1.19601)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.571209, 0.745895, -0.263604, 1.05731)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.456915, 0.6316, -0.148979, 0.942686)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.362457, 0.537142, -0.0542475, 0.847955)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.456915, 0.6316, -0.148979, 0.942686)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.571209, 0.745895, -0.263604, 1.05731)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.709505, 0.884191, -0.4023, 1.19601)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.876843, 1.05153, -0.570122, 1.36383)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.07932, 1.25401, -0.773187, 1.56689)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.32432, 1.49901, -1.01889, 1.8126)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.598297, 2.22503, -1.96636, 0.86514)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.894747, 2.52148, -2.26366, 1.16245)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.25345, 2.88019, -2.62341, 1.52219)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.68748, 3.31422, -3.05869, 1.95748)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.21266, 3.8394, -3.58539, 2.48417)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.84813, 4.47486, -4.2227, 3.12148)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.84813, 4.47486, -4.2227, 3.12148)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.21266, 3.8394, -3.58539, 2.48417)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.68748, 3.31422, -3.05869, 1.95748)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.25345, 2.88019, -2.62341, 1.52219)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.894747, 2.52148, -2.26366, 1.16245)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.598297, 2.22503, -1.96636, 0.86514)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.3824, 1.44093, -1.91554, 0.915961)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.1374, 1.19593, -1.66983, 0.670253)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.934925, 0.993447, -1.46676, 0.467188)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.767587, 0.826109, -1.29894, 0.299366)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.629291, 0.687813, -1.16024, 0.16067)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.514997, 0.573518, -1.04562, 0.0460452)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.420539, 0.47906, -0.950888, -0.048686)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.342475, 0.400996, -0.872598, -0.126976)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.487775, 0.255695, -0.56001, 0.185611)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.323356, 0.420115, -0.368825, 0.376796)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.25884, 0.355599, -0.304123, 0.312094)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.205521, 0.302279, -0.250649, 0.25862)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.161456, 0.258214, -0.206457, 0.214427)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.205521, 0.302279, -0.250649, 0.25862)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.25884, 0.355599, -0.304123, 0.312094)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.323356, 0.420115, -0.368825, 0.376796)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.401421, 0.498179, -0.447116, 0.455087)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.495879, 0.592637, -0.541847, 0.549818)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.610173, 0.706931, -0.656472, 0.664443)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.748469, 0.845227, -0.795168, 0.803139)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.915807, 1.01257, -0.96299, 0.970961)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.918286, 1.01009, -0.96299, 0.970961)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.682109, -0.289174, 0.671645)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.111818, 0.948932, 0.294999)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.21
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.4641
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.77156
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.14359
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.955669, -0.100244, -0.276852)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.179533, 0.943639, 0.278054)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.59374
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.993106, -0.111312, 0.0367563)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.0974959, 0.958411, 0.26822)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.59374
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.838046, -0.540388, 0.0752306)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.513379, 0.827705, 0.226597)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.59374
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.838046, -0.540388, 0.0752306)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.513379, 0.827705, 0.226597)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 3.13843
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.838046, -0.540388, 0.0752306)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.513379, 0.827705, 0.226597)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 3.7975
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.838046, -0.540388, 0.0752306)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.513379, 0.827705, 0.226597)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 4.59497
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.838046, -0.540388, 0.0752306)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.513379, 0.827705, 0.226597)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 5.55992
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.868583, -0.452106, 0.202889)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (-0.295762, 0.801472, 0.519776)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 5.55992
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 5.55992
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 4.59497
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 3.7975
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 3.13843
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.59374
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 2.14359
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.77156
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.4641
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1.21
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 1
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.826446
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.683013
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.564474
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.466507
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (-0.647455, -0.262939, 0.715308)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.235399, 0.823701, 0.515853)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.385543
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (0.187425, -0.137803, 0.972565)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.17356, 0.979179, 0.105293)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.385543
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (0.134262, -0.146286, 0.980089)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.0166557, 0.989237, 0.14537)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.385543
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View3DAtts = View3DAttributes()
View3DAtts.viewNormal = (0.134262, -0.146286, 0.980089)
View3DAtts.focus = (0.5175, 0.35, 0)
View3DAtts.viewUp = (0.0166557, 0.989237, 0.14537)
View3DAtts.viewAngle = 30
View3DAtts.parallelScale = 0.767076
View3DAtts.nearPlane = -1.53415
View3DAtts.farPlane = 1.53415
View3DAtts.imagePan = (0, 0)
View3DAtts.imageZoom = 0.385543
View3DAtts.perspective = 1
View3DAtts.eyeAngle = 2
View3DAtts.centerOfRotationSet = 0
View3DAtts.centerOfRotation = (0.5175, 0.35, 0)
View3DAtts.axis3DScaleFlag = 0
View3DAtts.axis3DScales = (1, 1, 1)
View3DAtts.shear = (0, 0, 1)
View3DAtts.windowValid = 1
SetView3D(View3DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.612652, 0.704452, -0.656472, 0.664443)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.498358, 0.590158, -0.541847, 0.549818)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.4039, 0.4957, -0.447116, 0.455087)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.325836, 0.417635, -0.368825, 0.376796)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.26132, 0.353119, -0.304123, 0.312094)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0440639, 0.570375, -0.370484, 0.245732)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.10858, 0.634891, -0.435187, 0.310435)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.186644, 0.712955, -0.513477, 0.388725)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.281102, 0.807413, -0.608209, 0.483456)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.395397, 0.921707, -0.722833, 0.598081)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.533692, 1.06, -0.861529, 0.736777)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.701031, 1.22734, -1.02935, 0.904599)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.90351, 1.42982, -1.23242, 1.10766)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.14851, 1.67482, -1.47812, 1.35337)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.90351, 1.42982, -1.23242, 1.10766)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.701031, 1.22734, -1.02935, 0.904599)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.533692, 1.06, -0.861529, 0.736777)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.395397, 0.921707, -0.722833, 0.598081)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.533692, 1.06, -0.861529, 0.736777)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.701031, 1.22734, -1.02935, 0.904599)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.90351, 1.42982, -1.23242, 1.10766)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.14851, 1.67482, -1.47812, 1.35337)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.44496, 1.97127, -1.77543, 1.65068)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.80366, 2.32997, -2.13517, 2.01042)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.23769, 2.76401, -2.57046, 2.44571)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.76287, 3.28918, -3.09716, 2.97241)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.89135, 4.16071, -4.03094, 2.03863)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.36617, 3.63553, -3.50424, 1.51193)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.89135, 4.16071, -4.03094, 2.03863)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.52681, 4.79618, -4.66824, 2.67593)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.52681, 4.79618, -4.66824, 2.67593)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.89135, 4.16071, -4.03094, 2.03863)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.36617, 3.63553, -3.50424, 1.51193)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.932135, 3.2015, -3.06895, 1.07664)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.573431, 2.8428, -2.70921, 0.7169)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.276982, 2.54635, -2.4119, 0.419593)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0319822, 2.30135, -2.1662, 0.173885)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.815009, 1.51832, -1.89019, 0.449894)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.815009, 1.51832, -1.89019, 0.449894)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.06001, 1.76332, -2.13589, 0.695602)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.35646, 2.05977, -2.4332, 0.99291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.06001, 1.76332, -2.13589, 0.695602)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.815009, 1.51832, -1.89019, 0.449894)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.61253, 1.31584, -1.68712, 0.246829)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.498477, 1.4299, -0.844117, 1.08983)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.331138, 1.26256, -0.676295, 0.922011)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.192843, 1.12426, -0.537599, 0.783315)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0785484, 1.00997, -0.422974, 0.66869)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.192843, 1.12426, -0.537599, 0.783315)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.331138, 1.26256, -0.676295, 0.922011)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.331138, 1.26256, -0.676295, 0.922011)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
SetActivePlots((0, 1))
SetActivePlots(0)
ChangeActivePlotsVar("Temperature")
DefineScalarExpression("operators/ConnectedComponents/mesh", "cell_constant(<mesh>, 0.)")
DefineCurveExpression("operators/DataBinning/1D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/2D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/3D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/Flux/mesh", "cell_constant(<mesh>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineVectorExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineVectorExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lagrangian/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/Lagrangian/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Density", "cell_constant(<Density>, 0.)")
DefineCurveExpression("operators/Lineout/Energy", "cell_constant(<Energy>, 0.)")
DefineCurveExpression("operators/Lineout/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure", "cell_constant(<Pressure>, 0.)")
DefineCurveExpression("operators/Lineout/Temperature", "cell_constant(<Temperature>, 0.)")
DefineCurveExpression("operators/Lineout/Mach", "cell_constant(<Mach>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineCurveExpression("operators/Lineout/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineCurveExpression("operators/Lineout/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineCurveExpression("operators/Lineout/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/ModelFit/model", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/ModelFit/distance", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/Poincare/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/Poincare/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineVectorExpression("operators/SurfaceNormal/mesh", "cell_constant(<mesh>, 0.)")
DefineScalarExpression("unnamed1", "")
DefineScalarExpression("u", "Momentum[0]")
ChangeActivePlotsVar("u")
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.329089, 1.26461, -0.672197, 0.926109)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.496427, 1.43194, -0.840019, 1.09393)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.698906, 1.63442, -1.04308, 1.297)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.943906, 1.87942, -1.28879, 1.5427)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.18521, 2.63812, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

DefineScalarExpression("v", "Momentum[1]/density")
DefineScalarExpression("u", "Momentum[0]/density")
DefineScalarExpression("operators/ConnectedComponents/mesh", "cell_constant(<mesh>, 0.)")
DefineCurveExpression("operators/DataBinning/1D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/2D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/3D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/Flux/mesh", "cell_constant(<mesh>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineVectorExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineVectorExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lagrangian/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/Lagrangian/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Density", "cell_constant(<Density>, 0.)")
DefineCurveExpression("operators/Lineout/Energy", "cell_constant(<Energy>, 0.)")
DefineCurveExpression("operators/Lineout/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure", "cell_constant(<Pressure>, 0.)")
DefineCurveExpression("operators/Lineout/Temperature", "cell_constant(<Temperature>, 0.)")
DefineCurveExpression("operators/Lineout/Mach", "cell_constant(<Mach>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineCurveExpression("operators/Lineout/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineCurveExpression("operators/Lineout/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineCurveExpression("operators/Lineout/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineCurveExpression("operators/Lineout/unnamed1", "cell_constant(unnamed1, 0.)")
DefineCurveExpression("operators/Lineout/u", "cell_constant(u, 0.)")
DefineScalarExpression("operators/ModelFit/model", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/ModelFit/distance", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/Poincare/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/Poincare/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/u", "cell_constant(<u>, 0.)")
DefineVectorExpression("operators/SurfaceNormal/mesh", "cell_constant(<mesh>, 0.)")
ChangeActivePlotsVar("v")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
DefineScalarExpression("v", "Momentum[1]/Density")
DefineScalarExpression("u", "Momentum[0]/Density")
DefineScalarExpression("operators/ConnectedComponents/mesh", "cell_constant(<mesh>, 0.)")
DefineCurveExpression("operators/DataBinning/1D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/2D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/DataBinning/3D/mesh", "cell_constant(<mesh>, 0)")
DefineScalarExpression("operators/Flux/mesh", "cell_constant(<mesh>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/IntegralCurve/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineVectorExpression("operators/LCS/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineVectorExpression("operators/LCS/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lagrangian/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/Lagrangian/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineCurveExpression("operators/LimitCycle/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/LimitCycle/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Density", "cell_constant(<Density>, 0.)")
DefineCurveExpression("operators/Lineout/Energy", "cell_constant(<Energy>, 0.)")
DefineCurveExpression("operators/Lineout/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure", "cell_constant(<Pressure>, 0.)")
DefineCurveExpression("operators/Lineout/Temperature", "cell_constant(<Temperature>, 0.)")
DefineCurveExpression("operators/Lineout/Mach", "cell_constant(<Mach>, 0.)")
DefineCurveExpression("operators/Lineout/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineCurveExpression("operators/Lineout/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineCurveExpression("operators/Lineout/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineCurveExpression("operators/Lineout/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineCurveExpression("operators/Lineout/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineCurveExpression("operators/Lineout/unnamed1", "cell_constant(unnamed1, 0.)")
DefineCurveExpression("operators/Lineout/u", "cell_constant(u, 0.)")
DefineScalarExpression("operators/ModelFit/model", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/ModelFit/distance", "point_constant(<mesh>, 0)")
DefineScalarExpression("operators/Poincare/Momentum", "cell_constant(<Momentum>, 0.)")
DefineScalarExpression("operators/Poincare/Skin_Friction_Coefficient", "cell_constant(<Skin_Friction_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Density", "cell_constant(<Density>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Energy", "cell_constant(<Energy>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Nu_Tilde", "cell_constant(<Nu_Tilde>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure", "cell_constant(<Pressure>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Temperature", "cell_constant(<Temperature>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Mach", "cell_constant(<Mach>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Pressure_Coefficient", "cell_constant(<Pressure_Coefficient>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Laminar_Viscosity", "cell_constant(<Laminar_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Heat_Flux", "cell_constant(<Heat_Flux>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Y_Plus", "cell_constant(<Y_Plus>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/Eddy_Viscosity", "cell_constant(<Eddy_Viscosity>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Sum/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Mean/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Variance/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Std. Dev./u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Slope/u", "cell_constant(<u>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/unnamed1", "cell_constant(<unnamed1>, 0.)")
DefineScalarExpression("operators/StatisticalTrends/Residuals/u", "cell_constant(<u>, 0.)")
DefineVectorExpression("operators/SurfaceNormal/mesh", "cell_constant(<mesh>, 0.)")
DrawPlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25249, 1.57901)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

ChangeActivePlotsVar("u")
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

SetActivePlots((0, 1))
SetActivePlots(1)
AddPlot("Pseudocolor", "v", 1, 1)
DrawPlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.940276, 1.88305, -1.25612, 1.57538)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.13993, 1.6834, -1.26338, 1.56812)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.13993, 1.6834, -1.26338, 1.56812)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.13993, 1.6834, -1.27064, 1.56086)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.14719, 1.67614, -1.27064, 1.56086)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.1363, 1.68703, -1.27064, 1.56086)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.1363, 1.68703, -1.27064, 1.56086)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.1363, 1.68703, -1.27064, 1.56086)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.891303, 1.44203, -1.02493, 1.31515)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.688824, 1.23955, -0.821868, 1.11208)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.521486, 1.07221, -0.654046, 0.94426)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.38319, 0.933914, -0.51535, 0.805564)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.268896, 0.81962, -0.400726, 0.690939)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.174438, 0.725162, -0.305994, 0.596208)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0963735, 0.647097, -0.227704, 0.517918)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.142608, 0.886079, -0.277412, 0.46821)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0877701, 0.655701, -0.294619, 0.451003)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.293294, 0.450177, -0.295575, 0.450047)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.371358, 0.528241, -0.373865, 0.528337)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.465816, 0.622699, -0.468596, 0.623068)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.580111, 0.736993, -0.583221, 0.737693)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.718407, 0.875289, -0.721917, 0.876389)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.234816, 1.35888, -0.726015, 0.872291)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

SetActivePlots((1, 2))
SetActivePlots(1)
DrawPlots()
HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0965205, 1.22058, -0.587319, 0.733595)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0177736, 1.10629, -0.472694, 0.61897)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.112232, 1.01183, -0.377963, 0.524239)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.190296, 0.933767, -0.299673, 0.445949)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.332729, 1.0762, -0.515712, 0.22991)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.397245, 1.01168, -0.451009, 0.165207)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.450564, 0.958365, -0.397536, 0.111734)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.494629, 0.914299, -0.353343, 0.0675408)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.531047, 0.877882, -0.31682, 0.0310178)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.477533, 0.824368, -0.260631, 0.087207)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.507631, 0.794271, -0.230447, 0.0570228)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.532504, 0.769397, -0.205501, 0.0320771)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.553061, 0.74884, -0.184885, 0.0114608)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.527134, 0.722913, -0.13454, 0.0618058)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.544123, 0.705924, -0.117501, 0.0447675)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.558163, 0.691883, -0.10342, 0.0306863)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.502457, 0.636177, -0.0697217, 0.0643849)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.403597, 0.537317, -0.0683462, 0.0657604)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.389556, 0.551357, -0.0824274, 0.0798416)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.372567, 0.568346, -0.0994657, 0.0968798)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.35201, 0.588903, -0.120082, 0.117496)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.327137, 0.613777, -0.145028, 0.142442)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.297039, 0.643874, -0.175212, 0.172626)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.260622, 0.680292, -0.211735, 0.209149)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.216556, 0.724357, -0.255928, 0.253342)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.163237, 0.777676, -0.309401, 0.306815)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0987212, 0.842192, -0.374104, 0.371518)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0206568, 0.920257, -0.452394, 0.449808)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0738012, 1.01471, -0.547125, 0.544539)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.188095, 1.12901, -0.66175, 0.659164)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.326391, 1.2673, -0.800446, 0.79786)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.493729, 1.43464, -0.968268, 0.965682)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.696208, 1.63712, -1.17133, 1.16875)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.941208, 1.88212, -1.41704, 1.41446)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.23766, 2.17857, -1.71435, 1.71176)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.59636, 2.53727, -2.07409, 2.0715)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.03039, 2.97131, -2.50938, 2.50679)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.55557, 3.49649, -3.03608, 3.03349)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-2.03039, 2.97131, -2.50938, 2.50679)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.59636, 2.53727, -2.07409, 2.0715)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-1.23766, 2.17857, -1.71435, 1.71176)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.941208, 1.88212, -1.41704, 1.41446)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.696208, 1.63712, -1.17133, 1.16875)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.493729, 1.43464, -0.968268, 0.965682)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.326391, 1.2673, -0.800446, 0.79786)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SaveSession("/Users/j6hickey/.visit/crash_recovery.97672.session")
# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
OpenDatabase("localhost:/Users/j6hickey/Downloads/BL_SU2/flow.vtk", 0)
SetActivePlots((1, 2))
SetActivePlots(2)
SetActivePlots((1, 2))
SetActivePlots(1)
HideActivePlots()
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.188095, 1.12901, -0.66175, 0.659164)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.0738012, 1.01471, -0.547125, 0.544539)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0206568, 0.920257, -0.452394, 0.449808)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.0987212, 0.842192, -0.374104, 0.371518)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.163237, 0.777676, -0.309401, 0.306815)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.216556, 0.724357, -0.255928, 0.253342)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.260622, 0.680292, -0.211735, 0.209149)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.297039, 0.643874, -0.175212, 0.172626)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.327137, 0.613777, -0.145028, 0.142442)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.35201, 0.588903, -0.120082, 0.117496)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.372567, 0.568346, -0.0994657, 0.0968798)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (0.389556, 0.551357, -0.0824274, 0.0798416)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

